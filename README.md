# nomadreservations/pipelines-kubectl-doctl

Build image based on `atlassian/default-image:2` that adds kubectl and doctl. Useful for managing deployments to your [Digitalocean](https://digitalocean.com) Kubernetes clusters.

## Getting Started

These instructions will cover usage information and for the docker container 

### Prerequisities


In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

## Find us at

* [Bitbucket](https://bitbucket.org/campgroundbooking/nomadreservations-pipelines-kubectl-doctl)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
